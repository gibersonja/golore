package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
)

func main() {

	var files []string
	var dirs []string
	var ext string = "txt"

	args := os.Args[1:]

	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "--help" || arg == "-h" {
			exe, err := os.Executable()
			er(err)

			fmt.Print("-h | --help\t\t\tPrint this help message\n")
			fmt.Print("-e <EXT> | --extension <EXT>\tSpecify extension to insert (Default: txt)\n")
			fmt.Printf("\nEXAMPLE:\n%s file1 file2 dir1 dir2 -e pdf\n", filepath.Base(exe))
			return
		}

		if (arg == "--extension" || arg == "-e") && n+1 < len(args) {
			ext = args[n+1]
		}

		fh, err := os.Open(arg)
		er(err)
		if err != nil {
			continue
		}
		defer fh.Close()

		fhInfo, err := fh.Stat()
		er(err)
		if err != nil {
			continue
		}

		if fhInfo.IsDir() {
			dirs = append(dirs, arg)
		} else {

			path, err := filepath.Abs(arg)
			er(err)
			if err != nil {
				continue
			}

			files = append(files, path)
		}
	}

	for i := 0; i < len(dirs); i++ {
		dir := dirs[i]
		entries, err := os.ReadDir(dir)
		er(err)
		if err != nil {
			continue
		}

		for _, e := range entries {
			path, err := filepath.Abs(dir)
			er(err)
			if err != nil {
				continue
			}

			fh, err := os.Open(filepath.Join(path, e.Name()))
			er(err)
			if err != nil {
				continue
			}
			defer fh.Close()

			fhInfo, err := fh.Stat()
			er(err)
			if err != nil {
				continue
			}

			if !fhInfo.IsDir() {
				files = append(files, filepath.Join(path, e.Name()))
			}
		}
	}

	for j := 0; j < len(files); j++ {
		file := files[j]

		regex := regexp.MustCompile(`^(.+)\.html$`)

		if regex.MatchString(file) {
			new_name := regex.FindStringSubmatch(file)[1] + "." + ext + ".html"
			fmt.Printf("RENAME: %s -> %s\n", file, new_name)

			fmt.Printf("\tREAD:   %s\n", file)
			origFile, err := os.ReadFile(file)
			er(err)
			if err != nil {
				continue
			}

			fmt.Printf("\tCREATE: %s\n", new_name)
			newFile, err := os.Create(new_name)
			er(err)
			if err != nil {
				continue
			}

			fmt.Printf("\tCOPY:   %s to %s\n", file, new_name)
			fmt.Fprintf(newFile, "%s", string(origFile))

			fmt.Printf("\tREMOVE: %s\n", file)
			err = os.Remove(file)
			er(err)
			if err != nil {
				continue
			}
		}
	}

}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		fmt.Printf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(),
			path.Base(file), line, err)
	}
}
